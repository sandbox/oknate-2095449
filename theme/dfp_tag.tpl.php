<?php
  /**
   * @file
   * Default template for dfp tags.
   */
?>

<div <?php print drupal_attributes($placeholder_attributes) ?>>
  <?php if (isset($slug)):
    print drupal_render($slug);
  endif; ?>
  <?php  $async = variable_get('dfp_async_rendering', 0);
  
  if($async): ?>
    <script type="text/javascript">
      ybotq.push(function() {
        googletag.cmd.push(function() {
          googletag.display("<?php print $tag->placeholder_id ?>");
        });
      });
    </script>
  <?php else: ?>
    <script type="text/javascript">
      googletag.cmd.push(function() {
        googletag.display("<?php print $tag->placeholder_id ?>");
      });
    </script>
  <?php endif; ?>
</div>
